/// @desc Move the raygun to the player location

// Draw Raygun appropriately
var gun_offset = 15;
// If player moves left, we flip the sprite
if(obj_player.hsp != 0){
	// This line needs to be fixed
	//image_xscale = sign(obj_player.hsp);
	gun_offset = sign(obj_player.hsp) * gun_offset;
}
// If player runs left, the gun should be offset left instead of right
x = obj_player.x + gun_offset;
y = obj_player.y;
image_angle = point_direction(x, y, mouse_x, mouse_y);
if (image_angle > 90) and (image_angle < 270){
	// flip gun back up right
	image_yscale = -1;	
} else {
	image_yscale = 1;
}

// Shoot lazers
firingdelay -= 1;
recoil = max(0, recoil-1);
key_shoot = mouse_check_button(mb_left);
if(key_shoot and (firingdelay <= 0)){
	recoil = 4;
	firingdelay = 20;
	with (instance_create_layer(x + 16, y + 2, "Lazers", obj_lazer)){
		speed = 10;
		direction = other.image_angle + random_range(-3, 3);
		image_angle = direction;
	}
}
// Give raygun slight recoil
x -= lengthdir_x(recoil, image_angle);
y -= lengthdir_y(recoil, image_angle);