{
    "id": "6aa20ece-0a54-4a29-9278-3156b518516a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_raygun",
    "eventList": [
        {
            "id": "5a9ecc4c-31ad-4006-8605-abcf118c6d16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6aa20ece-0a54-4a29-9278-3156b518516a"
        },
        {
            "id": "0c31b1a8-c246-4b28-ac92-fb3dca39cc07",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6aa20ece-0a54-4a29-9278-3156b518516a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26e8e5bb-bf5f-435f-ae92-d3c9b592512d",
    "visible": true
}