{
    "id": "605ecff9-e5ac-44b9-b9c8-18b0c4bdb826",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGhost",
    "eventList": [
        {
            "id": "978b04f9-4d7a-4045-9c6d-6ad580473310",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "605ecff9-e5ac-44b9-b9c8-18b0c4bdb826"
        },
        {
            "id": "ea6527cb-c3cd-4b56-813e-c96b5ccddd62",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "605ecff9-e5ac-44b9-b9c8-18b0c4bdb826"
        },
        {
            "id": "3e389894-875d-4b95-820b-c1861b7e2938",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "605ecff9-e5ac-44b9-b9c8-18b0c4bdb826"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "878c3180-2c32-4c63-a4bf-4233646d8451",
    "visible": true
}