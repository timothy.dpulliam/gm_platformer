/// @descr core player logic

// Get player input
// Is key currently being pressed?
key_left = (keyboard_check(vk_left) or keyboard_check(ord("A")));
key_right = keyboard_check(vk_right) or keyboard_check(ord("D"));
// Was key pressed just now?
key_jump = keyboard_check_pressed(vk_space);

// Calculate movement (boolean variables are either 0 or 1)
// Moving to right, _move == 1
// Moving to left, _move == -1
// standing still, _move == 0
var _move = key_right - key_left;
hsp = _move * walksp;
vsp += grv;

// We can only jump if the pixel directly below us is the floor
if(place_meeting(x, y+1, obj_invisible_wall) && key_jump){
	vsp -= jumpsp;
}	

// Horizontal collision
// If a wall is detected within the next left or right movement
if(place_meeting(x+hsp,y, obj_invisible_wall)){
	// While we can stil move left or right one pixel, keep doing that
	// sign(hsp) == -1 or 0 or 1
	while(not place_meeting(x+sign(hsp), y, obj_invisible_wall)){
			x += sign(hsp);
	}
	hsp = 0;
}
// Otherwise, if there is no horizontal collision
x += hsp;

// Vertical collision
if(place_meeting(x,y+vsp, obj_invisible_wall)){
	while(not place_meeting(x,y+sign(vsp), obj_invisible_wall)){
			y += sign(vsp);
	}
	vsp = 0;
}
// Otherwise, if there is no horizontal collision
y += vsp;

// Animations
// If not touching the floor
if(not place_meeting(x, y+1, obj_invisible_wall)){
    // If player is falling
	if(vsp > 0) sprite_index = spr_fall; else sprite_index = spr_jump;
// If we are touching the floor
} else {
	if(hsp = 0){
		sprite_index = spr_idle;
	} else {
		image_speed = 0.5;
		sprite_index = spr_run;
	}
}
// If player moves left, we flip the sprite
if(hsp != 0) image_xscale = sign(hsp);