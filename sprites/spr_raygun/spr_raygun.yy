{
    "id": "26e8e5bb-bf5f-435f-ae92-d3c9b592512d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_raygun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 8,
    "bbox_right": 27,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2aa3349a-60a6-4944-8a38-92e935cd634b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26e8e5bb-bf5f-435f-ae92-d3c9b592512d",
            "compositeImage": {
                "id": "2597328a-428c-4f41-bd5c-0dd2fd7c3ca6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2aa3349a-60a6-4944-8a38-92e935cd634b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ccf17e7-6f3a-4676-bae3-60fa886a6af6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2aa3349a-60a6-4944-8a38-92e935cd634b",
                    "LayerId": "c3b0fefc-2194-403b-a943-715b2204a5c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "c3b0fefc-2194-403b-a943-715b2204a5c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26e8e5bb-bf5f-435f-ae92-d3c9b592512d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 12,
    "yorig": 14
}