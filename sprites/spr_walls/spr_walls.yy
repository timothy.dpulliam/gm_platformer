{
    "id": "c81d4073-225c-4904-bd52-c39565218b39",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_walls",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8605eea8-e0cc-41ad-b0da-cdc1dab49d0d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c81d4073-225c-4904-bd52-c39565218b39",
            "compositeImage": {
                "id": "8c7412b9-0ee4-445d-9a69-e2db80140ca8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8605eea8-e0cc-41ad-b0da-cdc1dab49d0d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44bf27a8-fbcf-4582-94c1-21d3ff712098",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8605eea8-e0cc-41ad-b0da-cdc1dab49d0d",
                    "LayerId": "41fe915a-c559-4122-9948-e2e096e94000"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "41fe915a-c559-4122-9948-e2e096e94000",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c81d4073-225c-4904-bd52-c39565218b39",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}