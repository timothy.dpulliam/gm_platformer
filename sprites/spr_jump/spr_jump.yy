{
    "id": "48eb9ab4-ae8c-4357-a29c-e168616d8024",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 18,
    "bbox_right": 44,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "349ce084-d2fd-41ad-a512-ccdafe8c91ba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "48eb9ab4-ae8c-4357-a29c-e168616d8024",
            "compositeImage": {
                "id": "791ef12d-1d1d-4b84-905f-d6d319c06a87",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "349ce084-d2fd-41ad-a512-ccdafe8c91ba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "494b17f8-d8f8-4a6a-a20d-1fe1fad3d96b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "349ce084-d2fd-41ad-a512-ccdafe8c91ba",
                    "LayerId": "ff38e30e-e98c-406e-83d2-553c270623d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ff38e30e-e98c-406e-83d2-553c270623d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "48eb9ab4-ae8c-4357-a29c-e168616d8024",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 48
}