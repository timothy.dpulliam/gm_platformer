{
    "id": "75e3c23b-4feb-4c1f-8ca4-2e113df33fc5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_invisible_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c054c6d-e653-48da-bdf2-a07913426f66",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75e3c23b-4feb-4c1f-8ca4-2e113df33fc5",
            "compositeImage": {
                "id": "e7ad470c-9848-416b-ad92-3ceb06c5d19d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c054c6d-e653-48da-bdf2-a07913426f66",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a7efda7-a2e3-4327-bbb9-7dc4ce4ff27d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c054c6d-e653-48da-bdf2-a07913426f66",
                    "LayerId": "8e262b60-604c-444c-9c40-6c9699cfa7a8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8e262b60-604c-444c-9c40-6c9699cfa7a8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75e3c23b-4feb-4c1f-8ca4-2e113df33fc5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}