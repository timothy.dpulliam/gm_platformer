{
    "id": "06c6f727-fbdb-4009-8c49-8e36e694aad6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lazer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 2,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "37e126a4-4592-4322-af09-6881c50f46f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "06c6f727-fbdb-4009-8c49-8e36e694aad6",
            "compositeImage": {
                "id": "4825324c-ca84-46d7-8691-0f8f1421d56e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37e126a4-4592-4322-af09-6881c50f46f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "74700e02-53c1-4368-85c8-d6810c4695e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37e126a4-4592-4322-af09-6881c50f46f1",
                    "LayerId": "7ff6773d-0268-4ba2-8b61-88b40b9e0732"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 3,
    "layers": [
        {
            "id": "7ff6773d-0268-4ba2-8b61-88b40b9e0732",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "06c6f727-fbdb-4009-8c49-8e36e694aad6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 1
}