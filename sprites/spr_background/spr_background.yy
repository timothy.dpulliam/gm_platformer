{
    "id": "95aeb339-4024-453d-8cd9-f1a342c8d589",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 961,
    "bbox_left": 0,
    "bbox_right": 1052,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "86240957-ad89-4a3d-84a6-87eab13fc6a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "95aeb339-4024-453d-8cd9-f1a342c8d589",
            "compositeImage": {
                "id": "3800612c-fb04-46ff-9cf0-3ad7c0b7fc5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86240957-ad89-4a3d-84a6-87eab13fc6a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bafa2084-c99a-451c-b0d8-195fa7296558",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86240957-ad89-4a3d-84a6-87eab13fc6a5",
                    "LayerId": "7917ec69-f1c5-49b7-9966-594d9ddb3fba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 962,
    "layers": [
        {
            "id": "7917ec69-f1c5-49b7-9966-594d9ddb3fba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "95aeb339-4024-453d-8cd9-f1a342c8d589",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1053,
    "xorig": 526,
    "yorig": 481
}