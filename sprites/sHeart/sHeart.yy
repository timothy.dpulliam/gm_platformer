{
    "id": "bd4ca274-93f9-4e80-bec6-9a12f785b355",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sHeart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7c20bf2d-d8f2-4e18-a5d6-3689e7e51476",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bd4ca274-93f9-4e80-bec6-9a12f785b355",
            "compositeImage": {
                "id": "5ef111a4-0e7f-4fd0-ba24-f0974abcb764",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7c20bf2d-d8f2-4e18-a5d6-3689e7e51476",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184c715c-9e93-4822-bb78-e97f22c65d3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7c20bf2d-d8f2-4e18-a5d6-3689e7e51476",
                    "LayerId": "d0ea1219-3d7c-49f0-89b6-79c5b5593c5d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 46,
    "layers": [
        {
            "id": "d0ea1219-3d7c-49f0-89b6-79c5b5593c5d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bd4ca274-93f9-4e80-bec6-9a12f785b355",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 49,
    "xorig": 24,
    "yorig": 23
}