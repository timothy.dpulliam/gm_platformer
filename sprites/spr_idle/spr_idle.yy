{
    "id": "d33d3c6b-795c-4eed-8644-b9e41b6ef2db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 44,
    "bbox_top": 21,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbdde034-ede5-4012-b514-57126294b941",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d33d3c6b-795c-4eed-8644-b9e41b6ef2db",
            "compositeImage": {
                "id": "90596507-18cb-48d7-a375-7ff651b8125d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbdde034-ede5-4012-b514-57126294b941",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eafbd2b8-beca-4f87-b559-382485f192bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbdde034-ede5-4012-b514-57126294b941",
                    "LayerId": "84e6931d-8bad-4ba7-9fb3-a68ab88b7c2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "84e6931d-8bad-4ba7-9fb3-a68ab88b7c2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d33d3c6b-795c-4eed-8644-b9e41b6ef2db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 48
}