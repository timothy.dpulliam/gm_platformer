{
    "id": "21b59a47-e389-46ef-8a43-3654e0151080",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_idle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 15,
    "bbox_right": 48,
    "bbox_top": 7,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07d66ba6-1326-4041-af7f-6ecdc51e9f11",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "compositeImage": {
                "id": "9a999a49-6560-438a-9ebf-c3251aa236d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07d66ba6-1326-4041-af7f-6ecdc51e9f11",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b77cc2a-b013-41ee-afd4-559e59be1a73",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07d66ba6-1326-4041-af7f-6ecdc51e9f11",
                    "LayerId": "d91ca5a1-8099-4ceb-89e9-c250579fa240"
                }
            ]
        },
        {
            "id": "2634c71c-36d6-48ea-86a5-07e7ae2eaca2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "compositeImage": {
                "id": "249cad9f-99f9-4796-b3e3-1660f6bf4201",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2634c71c-36d6-48ea-86a5-07e7ae2eaca2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a7ac3229-d706-4e62-874a-d9f13638e5ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2634c71c-36d6-48ea-86a5-07e7ae2eaca2",
                    "LayerId": "d91ca5a1-8099-4ceb-89e9-c250579fa240"
                }
            ]
        },
        {
            "id": "f352c692-3918-4256-84df-9a6b8e2cf568",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "compositeImage": {
                "id": "3f3180c2-fa08-4727-83d5-819bfd4b0689",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f352c692-3918-4256-84df-9a6b8e2cf568",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "723c5fd2-9fda-491a-9c81-b0e52e553d5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f352c692-3918-4256-84df-9a6b8e2cf568",
                    "LayerId": "d91ca5a1-8099-4ceb-89e9-c250579fa240"
                }
            ]
        },
        {
            "id": "ee976fef-5b79-4241-9e35-a1a53f8376f0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "compositeImage": {
                "id": "c803612b-e9da-4f29-8198-00e1181a8f0a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee976fef-5b79-4241-9e35-a1a53f8376f0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4eb6d12b-435a-4583-a0aa-41035a955a4e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee976fef-5b79-4241-9e35-a1a53f8376f0",
                    "LayerId": "d91ca5a1-8099-4ceb-89e9-c250579fa240"
                }
            ]
        },
        {
            "id": "d61203d6-64e6-4efa-8a19-5b9b6f000ff3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "compositeImage": {
                "id": "bde18729-2344-4ec9-9775-0462a9fb250e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d61203d6-64e6-4efa-8a19-5b9b6f000ff3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e6829f1b-6237-4896-8c5d-ccb06da2ee99",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d61203d6-64e6-4efa-8a19-5b9b6f000ff3",
                    "LayerId": "d91ca5a1-8099-4ceb-89e9-c250579fa240"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "d91ca5a1-8099-4ceb-89e9-c250579fa240",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "21b59a47-e389-46ef-8a43-3654e0151080",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}