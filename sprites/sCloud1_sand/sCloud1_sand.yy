{
    "id": "a57c7547-0316-41f6-b529-99beb47e5ade",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sCloud1_sand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 201,
    "bbox_left": 0,
    "bbox_right": 710,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd54f806-9a0f-4664-a22d-de25f1e8957a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a57c7547-0316-41f6-b529-99beb47e5ade",
            "compositeImage": {
                "id": "affcd9af-5ff9-4676-804f-8581deb4939c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd54f806-9a0f-4664-a22d-de25f1e8957a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b18c4aaa-8a07-4c17-b0a1-ccf25b4aaf3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd54f806-9a0f-4664-a22d-de25f1e8957a",
                    "LayerId": "42bbb542-5b77-4ab5-bb81-6e4e0fbd6fb9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 202,
    "layers": [
        {
            "id": "42bbb542-5b77-4ab5-bb81-6e4e0fbd6fb9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a57c7547-0316-41f6-b529-99beb47e5ade",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 711,
    "xorig": 0,
    "yorig": 0
}