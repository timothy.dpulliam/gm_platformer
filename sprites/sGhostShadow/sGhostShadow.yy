{
    "id": "175575f3-260a-45fb-ba07-9fc59c3e0c1e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sGhostShadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 3,
    "bbox_left": 0,
    "bbox_right": 41,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1fb176a7-f22d-4e09-b188-2ef57219584e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "175575f3-260a-45fb-ba07-9fc59c3e0c1e",
            "compositeImage": {
                "id": "1337577c-9320-420b-889b-257f515c97d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1fb176a7-f22d-4e09-b188-2ef57219584e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9380cbf6-e2ee-4a97-bef5-bb077cf96d5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1fb176a7-f22d-4e09-b188-2ef57219584e",
                    "LayerId": "cedac839-469c-46e3-affb-2c2047542ab3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 4,
    "layers": [
        {
            "id": "cedac839-469c-46e3-affb-2c2047542ab3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "175575f3-260a-45fb-ba07-9fc59c3e0c1e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 42,
    "xorig": 21,
    "yorig": 2
}