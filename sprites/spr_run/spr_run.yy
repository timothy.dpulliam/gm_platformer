{
    "id": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_run",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 45,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "89b6053f-f39d-4e6e-a32c-1b727ed384f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "compositeImage": {
                "id": "544bed32-507b-469d-a80a-d104a41a3ca5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89b6053f-f39d-4e6e-a32c-1b727ed384f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a572556-63e4-4add-9eca-50fb685db188",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89b6053f-f39d-4e6e-a32c-1b727ed384f3",
                    "LayerId": "7d199a40-8d22-46dd-b849-9596263ecb8e"
                }
            ]
        },
        {
            "id": "fbf71036-d150-4e69-ad30-65eaaafbb97e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "compositeImage": {
                "id": "46a6221a-6a63-417e-8890-2af1ae51f599",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbf71036-d150-4e69-ad30-65eaaafbb97e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c975ff13-e8e2-44a6-b445-981a9b77a1e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbf71036-d150-4e69-ad30-65eaaafbb97e",
                    "LayerId": "7d199a40-8d22-46dd-b849-9596263ecb8e"
                }
            ]
        },
        {
            "id": "9c222da0-5340-4724-8529-7aea765db3f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "compositeImage": {
                "id": "3270c0dc-43a6-4405-b4a9-f4a131c16b70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c222da0-5340-4724-8529-7aea765db3f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9043aa4e-429a-4a24-9def-7844c53e992a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c222da0-5340-4724-8529-7aea765db3f8",
                    "LayerId": "7d199a40-8d22-46dd-b849-9596263ecb8e"
                }
            ]
        },
        {
            "id": "d7640cec-0b33-420e-9925-c90d7c8412b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "compositeImage": {
                "id": "865eaccb-e095-4774-a701-e956bf6a4922",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7640cec-0b33-420e-9925-c90d7c8412b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "027aea5b-7a71-4dc0-b23a-f14a66ccab3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7640cec-0b33-420e-9925-c90d7c8412b2",
                    "LayerId": "7d199a40-8d22-46dd-b849-9596263ecb8e"
                }
            ]
        },
        {
            "id": "0ca9f76f-8cc8-45f6-aec7-24b8197ce491",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "compositeImage": {
                "id": "0c41b046-d3cf-4a2c-9d1b-f561e7c8926b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca9f76f-8cc8-45f6-aec7-24b8197ce491",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bc7ed25-4afd-46bd-a80e-dfbe63761a55",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca9f76f-8cc8-45f6-aec7-24b8197ce491",
                    "LayerId": "7d199a40-8d22-46dd-b849-9596263ecb8e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7d199a40-8d22-46dd-b849-9596263ecb8e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9929b30e-bcae-4fe8-a76c-85631eeb55f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 48
}