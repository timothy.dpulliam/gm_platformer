{
    "id": "ed36d8bf-6dae-4aa3-8bd7-8e62b8279b99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sMoon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 424,
    "bbox_left": 0,
    "bbox_right": 426,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4e304ba9-33ad-4b42-8f35-a07266674ca8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ed36d8bf-6dae-4aa3-8bd7-8e62b8279b99",
            "compositeImage": {
                "id": "df58dc2c-e9bb-41d4-ab08-171d78fe69b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e304ba9-33ad-4b42-8f35-a07266674ca8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c5c1605-c5a3-44af-b7b0-4a377478331c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e304ba9-33ad-4b42-8f35-a07266674ca8",
                    "LayerId": "e3c5f758-2166-4d50-8898-c41620241d6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 425,
    "layers": [
        {
            "id": "e3c5f758-2166-4d50-8898-c41620241d6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ed36d8bf-6dae-4aa3-8bd7-8e62b8279b99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 427,
    "xorig": 0,
    "yorig": 0
}