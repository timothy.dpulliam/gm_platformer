{
    "id": "2a39d6c5-496e-4f36-a81d-4bdf89afb97b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGGrad_grass",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a0e0fa6-b77c-4049-af61-99ff9acb5eb0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a39d6c5-496e-4f36-a81d-4bdf89afb97b",
            "compositeImage": {
                "id": "8f3130a0-6ae5-4adc-a6d0-33a7ab355b0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a0e0fa6-b77c-4049-af61-99ff9acb5eb0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5be6e856-696d-49ef-aa25-cd0884883ab6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a0e0fa6-b77c-4049-af61-99ff9acb5eb0",
                    "LayerId": "48d956c1-f93f-445e-9188-db01310ad030"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "48d956c1-f93f-445e-9188-db01310ad030",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a39d6c5-496e-4f36-a81d-4bdf89afb97b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}