{
    "id": "fe79d750-5bd9-4253-a814-670188113c94",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 17,
    "bbox_right": 44,
    "bbox_top": 20,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eb1065bb-d94a-4b17-ab5c-b5455e6e9f02",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe79d750-5bd9-4253-a814-670188113c94",
            "compositeImage": {
                "id": "6f52e856-7e2c-43ea-a96d-863afbb76844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb1065bb-d94a-4b17-ab5c-b5455e6e9f02",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d466f1d-2dd1-4e0d-8e46-df9d921a0bc9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb1065bb-d94a-4b17-ab5c-b5455e6e9f02",
                    "LayerId": "77ed431c-7d31-46b3-888d-04421ed1613c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "77ed431c-7d31-46b3-888d-04421ed1613c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe79d750-5bd9-4253-a814-670188113c94",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 48
}