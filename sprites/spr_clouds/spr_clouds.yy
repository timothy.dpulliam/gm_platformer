{
    "id": "42a8f807-d034-4521-8394-f502c40c6eb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_clouds",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 488,
    "bbox_left": 27,
    "bbox_right": 1893,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "672fd0a9-f286-407c-9c43-7859690aaeca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "42a8f807-d034-4521-8394-f502c40c6eb6",
            "compositeImage": {
                "id": "f385d670-31cc-47af-b43c-588bf0f8cc0f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "672fd0a9-f286-407c-9c43-7859690aaeca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3933ef9e-7fcd-47fc-bcd3-61c89015b52e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "672fd0a9-f286-407c-9c43-7859690aaeca",
                    "LayerId": "b08ad921-6b97-4ea7-b7b4-52556c754d65"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "b08ad921-6b97-4ea7-b7b4-52556c754d65",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "42a8f807-d034-4521-8394-f502c40c6eb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 250
}