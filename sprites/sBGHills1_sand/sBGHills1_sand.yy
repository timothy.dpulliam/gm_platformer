{
    "id": "ebee2071-d628-4a65-9dda-114fb4a94a2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sBGHills1_sand",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 804,
    "bbox_left": 0,
    "bbox_right": 1023,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e133ca9c-b61d-4b6b-a43a-77bc7fc709ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ebee2071-d628-4a65-9dda-114fb4a94a2c",
            "compositeImage": {
                "id": "fa1b09a6-377e-4200-b0b6-c4e63506c7e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e133ca9c-b61d-4b6b-a43a-77bc7fc709ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3080b9fd-c8de-4ef0-9adc-6f0640d4f998",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e133ca9c-b61d-4b6b-a43a-77bc7fc709ee",
                    "LayerId": "130915b5-42c0-4a8e-a756-21957ac0287a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 805,
    "layers": [
        {
            "id": "130915b5-42c0-4a8e-a756-21957ac0287a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ebee2071-d628-4a65-9dda-114fb4a94a2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1024,
    "xorig": 0,
    "yorig": 0
}