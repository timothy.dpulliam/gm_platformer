{
    "id": "860dafbd-ad99-4090-ae13-f08cfbd098b4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sSandTiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 0,
    "bbox_right": 895,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa4dcd52-b58a-49a6-8338-4939c38bb835",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "860dafbd-ad99-4090-ae13-f08cfbd098b4",
            "compositeImage": {
                "id": "027c0e16-6baf-4bf6-b07b-89d854d0205d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa4dcd52-b58a-49a6-8338-4939c38bb835",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5789aa27-c742-4984-a49d-25f2543e8571",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa4dcd52-b58a-49a6-8338-4939c38bb835",
                    "LayerId": "71c82d69-d449-45b0-a9e5-40ec8c9b2b24"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "71c82d69-d449-45b0-a9e5-40ec8c9b2b24",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "860dafbd-ad99-4090-ae13-f08cfbd098b4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 896,
    "xorig": 0,
    "yorig": 0
}