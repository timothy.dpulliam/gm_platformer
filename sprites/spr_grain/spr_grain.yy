{
    "id": "7207e897-597f-43e8-8339-47f3a3835c53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_grain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 800,
    "bbox_left": 0,
    "bbox_right": 1145,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83dd220d-605e-4785-8325-c420b884cb4d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7207e897-597f-43e8-8339-47f3a3835c53",
            "compositeImage": {
                "id": "9b1252d1-5744-4ea0-8ef3-d5acee6bea24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83dd220d-605e-4785-8325-c420b884cb4d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "393ffe8b-62d4-4765-9859-185e3720da90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83dd220d-605e-4785-8325-c420b884cb4d",
                    "LayerId": "fb5eff54-5485-47dd-a78d-07be4f05399c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 801,
    "layers": [
        {
            "id": "fb5eff54-5485-47dd-a78d-07be4f05399c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7207e897-597f-43e8-8339-47f3a3835c53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1146,
    "xorig": 573,
    "yorig": 400
}